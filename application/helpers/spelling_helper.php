<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/* 
 * This Source is Original From Me
 * And This Source Is Free From Me. Thanks
 * Name : Andhy Taslin
 * Job : Web Developer
 */

if(!function_exists('spelling')){
    function spelling($int, $first = TRUE){
	if(!is_numeric($int)){
            return FALSE;
	}else{
            
            $lang1 = array(
                '',
                'satu', 'dua', 'tiga', 'empat', 'lima',
                'enam', 'tujuh', 'delapan', 'sembilan', 'sepuluh',
                'sebelas'
            );
            
            $lang2 = array(
                'belas', 'puluh', 'seratus', 'ratus', 
                'seribu', 'ribu', 'juta',
            );
            
            $sprtr = array(
                '', '-', '/'
            );
            
            if($int < 12){
                
		if($first === TRUE){
                    return $lang1[$int];
		}else{
                    return ' ' . $sprtr[0] .' '. $lang1[$int];
		}
					
            }else if ($int < 20){
					
		return spelling($int - 10, TRUE) .' '. $sprtr[0] .' '. $lang2[0];
					
            }else if ($int < 100){
					
		return spelling($int / 10, TRUE)  .' '. $sprtr[0] .' '. $lang2[1] . spelling($int % 10, FALSE);
					
            }else if ($int < 200){
					
		if($first === TRUE){
                    return $lang2[2] .' '. $sprtr[0] .' '. spelling($int - 100, TRUE);
		}else{
                    return ' '. $sprtr[0] .' '. $lang2[2] .' '. $sprtr[0] .' '. spelling($int - 100, FALSE);
		}
					
            }else if($int < 1000){
					
		return spelling($int / 100, TRUE) .' '. $sprtr[0] .' '. $lang2[3] .' '. $sprtr[0] .' '. spelling($int % 100, FALSE);
           
            }else if($int < 2000){				
		
                if($first === TRUE){
                    return $lang2[4] .' '. $sprtr[0] .' '. spelling($int - 1000, TRUE);
                }else{
                    return ' '. $sprtr[0] .' '. $lang2[4] .' '. $sprtr[0] .' '. spelling($int - 1000, FALSE);
		}
					
            }else if($int < 1000000){
					
		return spelling($int / 1000, TRUE) .' '. $sprtr[0] .' '. $lang2[5] .' '. $sprtr[0] .' '. spelling($int % 1000, TRUE);
				
            }else if($int < 1000000000){
					
		return spelling($int / 1000000, TRUE) .' '. $sprtr[0] .' '. $lang2[6] .' '. $sprtr[0] .' '. spelling($int % 1000000, TRUE);
					
            }
	}
    }
}

