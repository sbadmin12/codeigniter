<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/* 
 * This Source is Original From Me
 * And This Source Is Free From Me. Thanks
 * Name : Andhy Taslin
 * Job : Web Developer
 */
      /**********************************************************************/
     /*********************************Caution !!!**************************/
    /*Don't try to load this function with save record function************/
   /*it's will be save the record at twice********************************/
  /*you can save the record first and redirect the link to this function*/
 /**********************************************************************/

if(!function_exists('downloadPdf')){
    function downloadPdf($path, $filename=NULL){
        if(file_exists($path)){
            header('Content-type: application/pdf');
            header('Content-Disposition: inline; filename="' . $filename . '"');
            header('Content-Transfer-Encoding: binary');
            header('Content-Length: ' . filesize($path));
            header('Accept-Ranges: bytes');

            //there is 2 options function can be use
            //@readfile(); or readfile();
            //file_get_contents();

            @readfile($path);
            exit;
        }else{
		Return FALSE;
	}
    }
}
if(!function_exists('autoDownloadPdf')){
    function autoDownloadPdf($path, $filename=NULL){
        if(file_exists($path)){
            header('Content-Description: File Transfer');
            header('Content-Type: application/pdf');
            header('Content-Disposition: attachment; filename="' . $filename . '"');
            header('Content-Transfer-Encoding: binary');
            header('Expires: 0');
            header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
            header('Pragma: public');
            header('Content-Length: ' . filesize($filepath));

            //You can check out header list are used by header_list() function
            //print_r(header_list());

            //there is 2 options function can be use
            //@readfile(); or readfile();
            //file_get_contents();

            @readfile($path);
        }else{
            return FALSE;
        }
    }
}